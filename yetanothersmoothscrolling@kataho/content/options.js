/*global Components*/
var yass_option_global = {};

function getelement(id) {
  return document.getElementById(id);
}

function notifybrowsers() {
  var wm = yass_option_global.wmediator;
  var wenum = wm.getEnumerator(null);
  while (wenum.hasMoreElements()) {
    var w = wenum.getNext();
    if (("yass" in w) && ("refreshPreferences" in w.yass)) {
      w.yass.refreshPreferences();
    }
  }
}

function updatebar(id) {
  getelement(id + "_s").setAttribute("value", getelement(id + "_t").value);
  // this automatically call slideshcange function
}

function sliderchange(event) {
  updatetextbyid(event.target.id.slice(0, -2));

  if (yass_option_global.initializing == false) {
    updategraph(getelement("inputmethodtabbox").selectedTab.id);
    onaccept();
  }
}


function updategraph(prefix) {
  if ((new Date()).getTime() - yass_option_global.lastupdategraph < 80) return;
  yass_option_global.lastupdategraph = (new Date()).getTime();

  var step = getelement(prefix + "step_t").value;
  var bdump = getelement(prefix + "bdumping_t").value / 890 + 0.01;
  var edump = (900 - getelement(prefix + "dumping_t").value) / 1000;
  var smoothing = getelement("smoothing").value;

  var c = getelement("grapharea").getContext("2d");
  c.fillStyle = "#fff";
  c.fillRect(0, 0, 240, 100);

  // draw guid line
  c.fillStyle = "#aaa";
  for (var t = 40; t < 240; t += 40) {
    c.fillRect(t, 0, 1, 100);
  }

  c.fillStyle = "#999";
  c.strokeStyle = "#000";
  c.beginPath();
  c.moveTo(500, 500);
  c.lineTo(0, 500);

  var dest = (60.0 / 300.0) * step + 40.0;
  var d = 0;
  var bdumpbase = bdump * 110.0;
  var tipw = 4;
  for (var t = 0; t <= 60; t++) { // eslint-disable-line no-redeclare
    if (d + 0.3 > dest) {
      break;
    }
    var bd;
    if (smoothing == 2)
      bd = Math.min(Math.max(((t + 1) / (bdumpbase)) + 0.2, 0.05), 1.0);
    else
      bd = Math.min(Math.max(((t + 1) / (bdumpbase)), 0.05), 1.0);
    d += (dest - d) * edump * bd;
    c.lineTo(t * tipw, 100 - (dest - d));
  }
  c.stroke();
  c.fill();
}

function updatetextbyid(id) {
  getelement(id + "_t").value = getelement(id + "_s").getAttribute("value");

  var elm = null;

  elm = getelement(id + "_tr");
  if (elm) elm.value = getelement(id + "_t").value / 100; // tr

  elm = getelement(id + "_ts");
  if (elm) elm.value = (getelement(id + "_t").value * (100.0 / 890.0)).toPrecision(3); // ts
}

function inittab(id) {
  var prefs = getelement("preset_" + id).value.split(",");

  yass_option_global.initializing = true;
  for (var i in idlist) {
    getelement(idlist[i] + "_t").value = prefs[i];
    updatebar(idlist[i]);
  }
  updategraph(getelement("inputmethodtabbox").selectedTab.id);
  yass_option_global.initializing = false;
}

function selecttab(id) {
  getelement("tabpanel").style.backgroundImage = "url('chrome://yass/content/" + id + "bg.png')";

  getelement("wheelcaption").style.color = colorlist[id];
  getelement("keyboardcaption").style.color = colorlist[id];

  // restore values
  inittab(id);

  getelement("selectedpreset").value = id;
  getelement("pane").writePreferences(true);
  notifybrowsers();
}

function selectmethodtab(prefixmain, prefixreset) { // eslint-disable-line no-unused-vars
  updategraph(prefixmain);
}

function onaccept() {
  var pref = new Array();
  for (var i in idlist) {
    pref.push(getelement(idlist[i] + "_t").value);
  }
  getelement("selectedpreset").value = getelement("tabbox").selectedTab.id;
  getelement("preset_" + getelement("selectedpreset").value).value = pref.toString();
  getelement("pane").writePreferences(true);
  notifybrowsers();

  return true;
}

function onapplymisc() { // eslint-disable-line no-unused-vars
  getelement("usekbd").value = getelement("usekbd_c").checked;
  getelement("usepagejump").value = getelement("usepagejump_c").checked;
  getelement("usewholejump").value = getelement("usewholejump_c").checked;
  getelement("edgetype").value = getelement("edgetypemenu").value;

  getelement("useflickemulation").value = getelement("useflickemulation_c").checked;

  getelement("panedisplay").writePreferences(true);
  notifybrowsers();
}

function onchangesmoothalg() { // eslint-disable-line no-unused-vars
  getelement("smoothing").value = getelement("smoothingmenu").value;

  getelement("panedisplay").writePreferences(true);
  notifybrowsers();
  updategraph(getelement("inputmethodtabbox").selectedTab.id);
}

function ondialogok() { // eslint-disable-line no-unused-vars
  getelement("panedisplay").writePreferences(true);
  getelement("paneblacklist").writePreferences(true);
  notifybrowsers();
  getelement("prefwindow_yass").lastSelected = "pane";
}

function ondialogcancel() { // eslint-disable-line no-unused-vars
  // restore backup value
  for (var i in paramlist) {
    getelement(paramlist[i]).value = yass_option_global.backup[paramlist[i]];
  }
  // save it
  getelement("panedisplay").writePreferences(true);
  getelement("pane").writePreferences(true);

  notifybrowsers();
  getelement("prefwindow_yass").lastSelected = "pane";
}

function closehelptips() {
  var tips = document.getElementsByTagName("tooltip");
  try {
    for (var i in tips) {
      tips[i].hidePopup();
    }
  }
  catch (e) {}
}

function onhelp(e) {
  closehelptips();
  var elm = "help_" + getelement("prefwindow_yass").currentPane.id;
  getelement(elm).openPopup(e.target, "before_start", 0, 0, false, null);
}

// onload //

var idlist = [
  "wheelstep", "wheeldumping", "wheelbdumping", "wheelaccel", "kbdstep", "kbddumping", "kbdbdumping", "kbdaccel"
];

var paramlist = [
  "preset_red", "preset_green", "preset_blue", "selectedpreset", "usekbd",
  "usepagejump", "usewholejump", "edgetype", "smoothing", "useflickemulation"
];

var colorlist = {
  red: "#cc0000",
  green: "#008800",
  blue: "#0000cc"
};


function onpaneload0(p) { // eslint-disable-line no-unused-vars
  // spacer for mac os
  var osString = Components.classes["@mozilla.org/xre/app-info;1"]
                    .getService(Components.interfaces.nsIXULRuntime).OS;
  if (osString != "Darwin") {
    getelement("spacer4mac1").setAttribute("height", "0");
    getelement("spacer4mac2").setAttribute("height", "0");
  }

  // global parameters
  yass_option_global.lastupdategraph = 0;
  yass_option_global.wmediator = Components.classes["@mozilla.org/appshell/window-mediator;1"].getService(Components.interfaces.nsIWindowMediator);

  // save current parameters
  var obj = {};
  for (var i in paramlist) {
    obj[paramlist[i]] = getelement(paramlist[i]).value;
  }
  yass_option_global.backup = obj;

  // regist help button behavior
  getelement("buttonhelp").addEventListener("click", onhelp, false);
  getelement("buttonhelp").addEventListener("blur", closehelptips, false);

  // listener on slider
  for (i in idlist) {
    getelement(idlist[i] + "_s").addEventListener("change", sliderchange, false);
  }

  // select preset tab
  var stab = getelement("selectedpreset").value;
  if (!stab) stab = getelement("tabbox").selectedTab.id;
  getelement("tabbox").selectedTab = getelement(stab);
  selecttab(stab);

  //setTimeout(function() {
  //var s = (getelement("tabbox").clientWidth - 15) + "px";
  //getelement("pixelscroll_d").style.width = s;
  //},10);
}
