/*global yass*/
var yassev = { // eslint-disable-line no-unused-vars

  prefobserver: null,

  update_indicator: function() {
    if (this.prefobserver == null) {
      this.prefobserver = { observe: yassev.refresh_tbb_enabled };
      yass.m_prefservice.addObserver("extensions.yass.enabled", this.prefobserver, false);
    }
  },

  onunload: function() {
    if (this.prefobserver != null) {
      yass.m_prefservice.removeObserver("extensions.yass.enabled", this.prefobserver);
    }
  },

  refresh_tbb_enabled: function() {
    var enabled = (yass.m_prefservice.getCharPref("extensions.yass.enabled") == "true") ? true : false;
    var tbb = document.getElementById("yass-tbb");
    if (tbb) {
      tbb.setAttribute("yass-graybutton", enabled ? "false" : "true");
    }
  },

  popup_showing: function(thi) {
    var presetid = yass.m_prefservice.getCharPref("extensions.yass.selectedpreset");
    var list = ["red", "green", "blue"];
    for (var i in list) {
      document.getElementById("yass-statusbarmenu-" + list[i]).hidden = (list[i].indexOf(presetid) >= 0);
    }

    document.getElementById("yass-statusbarmenu-enable").setAttribute(
      "checked",
      yass.m_prefservice.getCharPref("extensions.yass.enabled"));
  },

  tbb_popup_showing: function(thi) {
    var presetid = yass.m_prefservice.getCharPref("extensions.yass.selectedpreset");
    var list = ["red", "green", "blue"];
    for (var i in list)
      document.getElementById("yass-tbbmenu-" + list[i]).hidden = (list[i].indexOf(presetid) >= 0);
  },

  toggle_enable: function(sw) {
    yass.toggle_enable(sw);
  },

  show_preferences: function() {
    // show preferences dialog
    window.openDialog("chrome://yass/content/options.xul", "yass options", "chrome,dialog,toolbar");
  },

  end: 0
};
