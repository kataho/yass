/*global browser, util*/
var iframeStore = {};

browser.storage.local.get(["selected-preset", "output-log"]).then((val) => {
  // default preferences
  if (val["selected-preset"] == undefined) {
    let defaults = {
      "selected-preset": "green",
      "enabled": true,
      "blacklist": [],
      "use-cursor-keys": true,
      "use-page-jump-keys": false,
      "use-document-jump-keys": false,
      "bouncy-edge": 60,
      "bouncy-dege-shadow": false,
      "output-log": false,
      "bottom-bounce-quirk": false,
      "tick-wheel": 84,
      "page-margin": 40,

      "red-w-step": 70,
      "red-w-pre-smooth": 20,
      "red-w-post-smooth": 575,
      "red-w-accel": 70,
      "red-k-step": 100,
      "red-k-pre-smooth": 14,
      "red-k-post-smooth": 588,
      "red-k-accel": 100,
      "red-accel-travel": 1.0,

      "green-w-step": 60,
      "green-w-pre-smooth": 130,
      "green-w-post-smooth": 707,
      "green-w-accel": 163,
      "green-k-step": 86,
      "green-k-pre-smooth": 124,
      "green-k-post-smooth": 600,
      "green-k-accel": 105,
      "green-accel-travel": 1.0,

      "blue-w-step": 30,
      "blue-w-pre-smooth": 187,
      "blue-w-post-smooth": 792,
      "blue-w-accel": 416,
      "blue-k-step": 70,
      "blue-k-pre-smooth": 244,
      "blue-k-post-smooth": 823,
      "blue-k-accel": 110,
      "blue-accel-travel": 1.0
    };
    browser.storage.local.set(defaults);
  }
  else {
    util.dolog = val["output-log"];
    browser.storage.local.set({"enabled": true});
  }
});

// signal reset_design_mode_check_target to content script when its tab activated
browser.tabs.onActivated.addListener(function(info) {
  if (Object.keys(iframeStore).includes("" + info.tabId)) {
    browser.tabs.sendMessage(info.tabId, {type: "reset-design-mode-check-target"});
  }
});

browser.tabs.onRemoved.addListener(function(tabid) {
  delete iframeStore[tabid];
});

// display gray icon when disabled
browser.storage.onChanged.addListener(function(changes, area) {
  if (area != "local") return;

  if (Object.keys(changes).includes("enabled")) {
    let enableIcons = {
      "16": "img/wheel16.png",
      "32": "img/wheel.png"
    };
    let disableIcons = {
      "16": "img/wheel16gray.png",
      "32": "img/wheelgray.png"
    };

    browser.browserAction.setIcon({
      path: changes.enabled.newValue ? enableIcons : disableIcons
    });
  }

  if (Object.keys(changes).includes("output-log")) {
    util.dolog = changes["output-log"].newValue;
  }
});

browser.runtime.onMessage.addListener(function(message, sender, sendResponse) {
  // from preset button of options window and item of browser action popup
  if (message.type == "change-preset") {
    browser.storage.local.set({"selected-preset": message.value});
  }
  else if (message.type == "event-bubble") {
    // just relaying message between content scripts
    browser.tabs.sendMessage(sender.tab.id, message, {frameId: message.dest});
  }
  // frame attributes query relay
  else if (message.type == "frame-attributes") {
    browser.tabs.sendMessage(sender.tab.id, message, {frameId: message.dest}, function(result) {
      // connection between content scripts is available only one at a time, so
      setTimeout(function() {
        browser.tabs.sendMessage(sender.tab.id, {type: "frame-attributes-response", result: result}, {frameId: sender.frameId});
      }, 1);
    });
  }
});
