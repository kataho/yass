/*global browser*/
document.addEventListener("DOMContentLoaded", (e) => {
  for (let e of document.querySelectorAll("[i18n]")) {
    e.innerText = browser.i18n.getMessage(e.getAttribute("i18n"));
  }

  browser.storage.local.get(["enabled", "selected-preset"]).then((val) => {
    document.getElementById(val.enabled ? "item-enable" : "item-disable").style.display = "none";
    document.getElementById("item-" + val["selected-preset"]).style.display = "none";
  });

  //document.getElementById("version").innerText = browser.runtime.getManifest().version;
});

document.addEventListener("click", (e) => {
  let p = null;
  let target = e.target;
  while (!target.id && target.parentNode != null) target = target.parentNode;

  switch (target.id) {
    case "item-red":
      p = browser.runtime.sendMessage({type: "change-preset", value: "red"});
      break;

    case "item-green":
      p = browser.runtime.sendMessage({type: "change-preset", value: "green"});
      break;

    case "item-blue":
      p = browser.runtime.sendMessage({type: "change-preset", value: "blue"});
      break;

    case "item-options":
      p = browser.runtime.openOptionsPage();
      break;

    case "item-disable":
      p = browser.storage.local.set({enabled: false});
      break;

    case "item-enable":
      p = browser.storage.local.set({enabled: true});
      break;
  }

  if (p) p.then(window.close);
});
