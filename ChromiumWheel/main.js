/*global chrome,util,MatchingPatterns,Quirks,Scroller,NodeFinder,EventHandler,EventConverter*/
(function() {

  let frameElement = { scrolling: "", parent: -1 };
  let listenerLoaded = false; // also used for top frame response to children
  let parentInfoNeeded = true; // for child frames

  let loadListeners = function(doLoad) {
    if (doLoad == listenerLoaded) return;
    let regularElement = (document.documentElement || document.createElementNS("http://www.w3.org/1999/xhtml", "div"));
    let wheelevent = ("onwheel" in regularElement) ? "wheel" : "mousewheel";
    let switchingEventListener = doLoad ? addEventListener : removeEventListener;
    listenerLoaded = doLoad;

    switchingEventListener.call(window, wheelevent, EventHandler, {capture: Quirks.overrideWheelEvent(), passive: false});
    switchingEventListener.call(window, "mousedown", EventHandler, false);
    switchingEventListener.call(window, "keydown", EventHandler, false);
    switchingEventListener.call(window, "resize", EventHandler, false);
  };

  document.addEventListener("DOMContentLoaded", function() {

    let topframe = false;
    try { topframe = (document.defaultView.top.document === document); }
    catch (e) {}

    if (topframe) {
      // Chrome browser has no window.mozInnerScreenX/Y: a variable of a viewport position.
      // Problems expected on some layout which places content viewport on inordinary place (ex. developer tool inserted left or top)
      // v2.0.5 we no longer support browser zooming (detecting with devicePixelRatio which causes problem on retina display),
      // We have been left for a decade without a way to access zoom level. Nobody care of this means nobody use this.
      let elementFromScreenXY = function(screenX, screenY) {
        let w = document.defaultView;
        let borderSize = (w.outerWidth - w.innerWidth) / 2;
        let headerSize = (w.outerHeight - w.innerHeight) - borderSize;
        let pointx = screenX - (w.screenLeft + borderSize);
        let pointy = screenY - (w.screenTop + headerSize);
        return document.elementFromPoint(pointx, pointy);
      };

      // event listener only for top frame
      chrome.runtime.onMessage.addListener(function(message, sender, response) {
        if (message.type == "frame-attributes") {
          var frame = elementFromScreenXY(message.screenX, message.screenY);
          if (frame.nodeName != "FRAME" && frame.nodeName != "IFRAME") frame = null;
          util.dump("frame-attributes: (" + message.screenX + "," + message.screenY + ")" + (frame ? frame.nodeName : "failed!"));
          response((frame != null) ? {
            scrolling: "" + frame.getAttribute("scrolling"),
            parent: (frame.nodeName.toLowerCase() == "frame") ? -1 : 0,
            unloadListeners: !listenerLoaded
          } : {
            scrolling: "no", parent: 0, unloadListeners: true // failed to find out iframe element under mouse cursor
          });
        }
        else if (message.type == "event-bubble") {
          let ev = message.event;
          ev.preventDefault = function() {};
          ev.target = elementFromScreenXY(ev.screenX, ev.screenY);
          util.dump("event-bubble: (" + ev.screenX + "," + ev.screenY + ") " + (ev.target ? (ev.target.nodeName + "#" + ev.target.id) : "NULL"));
          EventHandler.handleEvent(ev);
        }
      });
    }
    else {
      frameElement = { scrolling: "no", parent: 0 };

      EventHandler.passToParentFrame = (event) => {
        chrome.runtime.sendMessage({type: "event-bubble", event: event, dest: frameElement.parent});
      };
      EventHandler.isChildFrame = () => { return true; };
      NodeFinder.isScrollableFrame = () => { return frameElement.scrolling.toLowerCase() != "no"; };
      NodeFinder.isChildFrame = () => { return true; };

      // frame attributes query and response listener
      addEventListener("mouseover", function(ev) {
        util.dump("mouseover on iframe");
        if (parentInfoNeeded == false) return;
        chrome.runtime.sendMessage({
          type: "frame-attributes",
          dest: 0,
          screenX: ev.screenX,
          screenY: ev.screenY
        });
      });

      chrome.runtime.onMessage.addListener(function(message) {
        if (message.type == "frame-attributes-response") {
          util.dump("frame-attributes-response: " + JSON.stringify(message.result));
          frameElement = message.result;
          parentInfoNeeded = false;
          if (message.result.unloadListeners) loadListeners(false);
        }
      });
    }
  });

  let refreshPreferences = function(prefs) {
    EventHandler.clearTarget();
    Scroller.clearTarget();

    let preset = prefs["selected-preset"] || "green";

    EventConverter.pref.wheelstep = prefs[preset + "-w-step"];
    EventConverter.pref.wheeldumping = (900 - prefs[preset + "-w-post-smooth"]) / 1000;
    EventConverter.pref.wheelbdumping = prefs[preset + "-w-pre-smooth"] / 890;
    EventConverter.pref.wheelaccel = prefs[preset + "-w-accel"];
    EventConverter.pref.kbdstep = prefs[preset + "-k-step"];
    EventConverter.pref.kbddumping = (900 - prefs[preset + "-k-post-smooth"]) / 1000;
    EventConverter.pref.kbdbdumping = prefs[preset + "-k-pre-smooth"] / 890;
    EventConverter.pref.kbdaccel = prefs[preset + "-k-accel"];
    EventConverter.pref.accelbytravel = prefs[preset + "-accel-travel"];

    EventHandler.pref.usekbd = true;
    EventHandler.pref.usepagejump = true;
    EventConverter.pref.pagemargin = prefs["page-margin"];
    EventHandler.pref.usewholejump = true;
    EventHandler.pref.recognizewheeltick = prefs["recognize-wheel-tick"];
    util.dolog = prefs["output-log"];
    Scroller.edgeSize = prefs["bouncy-edge"];
    EventHandler.pref.tickwheel = prefs["tick-wheel"];
  };

  const blacklistMatcher = new MatchingPatterns(document.URL);

  const updatePreferences = function(updateEnabledPrefs, updateScrollPrefs, getKeys) {
    const passedBlacklist = (blacklist) => {
      return (blacklist ?? []).find((pattern) => blacklistMatcher.test(pattern)) == undefined;
    };
    chrome.storage.local.get(getKeys, (prefs) => {
      if (updateEnabledPrefs) loadListeners(passedBlacklist(prefs.blacklist) && prefs.enabled);
      if (updateScrollPrefs) refreshPreferences(prefs);
    });
  };

  // storage change event listener
  chrome.storage.onChanged.addListener(function(changes, area) {
    if (area != "local") return;
    if ("tick-wheel" in changes) return;
    if ("blacklist" in changes) parentInfoNeeded = true; // need refresh for parent blacklist status

    // optimize, choose a fragment to load and update
    let changedEnabled = (("blacklist" in changes) || ("enabled" in changes));
    updatePreferences(changedEnabled, !changedEnabled, changedEnabled ? ["blacklist", "enabled"] : null);
  });

  setTimeout(() => updatePreferences(true, true, null), 10);

})();
