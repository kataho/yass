/*global chrome*/
chrome.runtime.onStartup.addListener(() => {
  chrome.storage.local.set({"enabled": true});
});

chrome.runtime.onInstalled.addListener(() => {
  chrome.storage.local.get(["selected-preset", "recognize-wheel-tick"], (val) => {
    if (val["selected-preset"] === undefined) {
      // initialize storage.local
      let settings = {
        "selected-preset": "green",
        "enabled": true,
        "blacklist": [],
        "bouncy-edge": 60,
        "output-log": false,
        "tick-wheel": 84,
        "page-margin": 40,
        "green-w-step": 40,
        "green-w-pre-smooth": 224,
        "green-w-post-smooth": 589,
        "green-w-accel": 204,
        "green-k-step": 86,
        "green-k-pre-smooth": 227,
        "green-k-post-smooth": 491,
        "green-k-accel": 105,
        "green-accel-travel": 1.0
      };
      chrome.storage.local.set(settings);
    }

    // migration v2.0.6
    if (val["recognize-wheel-tick"] === undefined) {
      chrome.storage.local.set({"recognize-wheel-tick": true});
    }
  });

  chrome.storage.local.set({"enabled": true});
});

chrome.storage.onChanged.addListener(function(changes, area) {
  if (area != "local") return;

  if (Object.keys(changes).includes("enabled")) {
    chrome.action.setIcon({
      "path": (changes.enabled.newValue) ? "wheel.png" : "wheel_disabled.png"
    });
  }
});

chrome.runtime.onMessage.addListener(function(request, sender, response) {
  if (sender.tab === undefined) return;

  // event bubble relay
  if (request.type == "event-bubble") {
    chrome.tabs.sendMessage(sender.tab.id, request, {frameId: request.dest});
  }
  // frame attributes query relay
  else if (request.type == "frame-attributes") {
    chrome.tabs.sendMessage(sender.tab.id, request, {frameId: request.dest}, function(result) {
      // connection between content scripts is available only one at a time, so
      setTimeout(function() {
        chrome.tabs.sendMessage(sender.tab.id, {type: "frame-attributes-response", result: result}, {frameId: sender.frameId});
      }, 1);
    });
  }
});

chrome.action.onClicked.addListener(function(t) {
  chrome.storage.local.get("enabled", function(prefs) {
    chrome.storage.local.set({"enabled": (prefs.enabled) ? false : true});
  });
});
