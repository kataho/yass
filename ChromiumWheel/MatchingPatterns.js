const MatchingPatterns = class {
  constructor(target) {
    this.target = target;
    this.cachedResult = {};
  }
  test(pattern) {
    return this.cachedResult[pattern] ?? ((_this) => {
      return _this.cachedResult[pattern] = RegExp(pattern).test(_this.target);
    })(this);
  }
};
