/*global util,ScrollerClasses*/
// based on the code in All-in-One Gestures
const NodeFinder = {
  isChildFrame: () => { return false; }, // will be injected by main.js of child frame
  isScrollableFrame: () => { return true; }, // will be injected by main.js of child frame

  findNodeToScroll: function(orig, hint, ev) {

    // 0 neither scrollable 1 horizontal only  2 vertical only 3 both
    function scrollType(wscroll, wclient, hscroll, hclient) {
      if (hclient < 50) return 0;
      if (hscroll - hclient < 10) hclient = hscroll; // too small to scroll
      let flag = 0;
      if (wscroll > wclient) flag += 1;
      if (hscroll > hclient) flag += 2;
      return flag;
    }

    let newScroller = (p) => {
      let editable = false;
      if (p.nodeName.toLowerCase() == "textarea") editable = true;
      let mode = (p.ownerDocument && p.ownerDocument.designMode) ? p.ownerDocument.designMode : "off";
      if (mode && mode.toLowerCase() == "on") editable = true;
      if (p.isContentEditable) editable = true;
      if (p.style["-webkit-user-modify"] != "") editable = true;
      if (editable || this.edgeSize == 0) return ScrollerClasses.ScrollerNoEdge;
      return ScrollerClasses.Scroller;
    };

    if (!orig.ownerDocument) {
      util.dump("ownerDocument unreachable");
      return null;
    }

    let doc = orig.ownerDocument.documentElement;
    if (doc && doc.nodeName.toLowerCase() != "html") {
      util.dump("doc is " + doc.nodeName + " not html\n");
      return null;
    }

    let bodies = doc.getElementsByTagName("body");
    if (!bodies || bodies.length == 0) {
      util.dump("no body\n");
      return null;
    }
    let body = bodies[0];

    let scrollElement = orig.ownerDocument.scrollingElement;
    if (!scrollElement) {
      util.dump("no scrollingElement");
      return null;
    }

    // true: scrollable / false: bottom most or top most
    let scrollOverCheck =
      (hint == 0) ? (() => true)
        : ((hint < 0) ? ((n, a, b) => (("_yass_ownedby" in n) && n._yass_ownedby) ? n._yass_ownedby.scrollovercheck(hint) : (a > 0))
                      : ((n, a, b) => (("_yass_ownedby" in n) && n._yass_ownedby) ? n._yass_ownedby.scrollovercheck(hint) : (a < b) && (b > 1)));
    //                there are some region unmovable really but looks 1px scrollable ------------------------------------------------------^

    // shadowDOM support
    let path = ev.composed ? ev.composedPath() : [];
    if (path.length == 0) {
      // mimic composedPath() result by an usual parentNode tracking
      for (let n = orig; n; n = n.parentNode) path.push(n);
    }
    path = path.filter(e => e instanceof Element);
    let node = path.shift();

    let bodyOverflowValue = null;
    let log = "";

    while (node) {
      if (node == doc || node == body) node = scrollElement;

      let nodename = node.nodeName.toLowerCase();

      /*log*/ log += nodename;

      if (/^(option|optgroup)$/.test(nodename)) {
        util.dump("option found :" + log);
        return null;
      }

      let overflowProp = getComputedStyle(node).getPropertyValue("overflow-y");
      if (node == body) bodyOverflowValue = overflowProp;

      if (node.clientWidth && node.clientHeight
          && (overflowProp != "hidden")
          && (node == doc || node == body || overflowProp != "visible")) {

        /*log*/ log += "(" + node.scrollTop + " " + node.clientHeight + " " + node.scrollHeight + ")";

        // scroll focus overflow
        if ((scrollType(0, 0, node.scrollHeight, node.clientHeight) >= 2)
            && scrollOverCheck(node, node.scrollTop, node.scrollHeight - node.clientHeight)) {
          // if this is in a unscrollable frame element
          if ((node == doc || node == body) && !this.isScrollableFrame()) {
            util.dump("unscrollable frame");
            return 2;
          }

          return new(newScroller(node))(orig, node, log);
        }
      }

      if (node == scrollElement) break;

      /*log*/ log += ">";

      node = path.shift();
    }

    if (this.isChildFrame()) {
      util.dump(log + " passing upper frame");
      return 2;
    }

    // no scrollable area found in content ( mainly for image only page to handle )

    if (scrollElement.scrollHeight - scrollElement.clientHeight > 1) {
      if (scrollElement == body && bodyOverflowValue == "hidden") {
        util.dump("DEFAULT but hidden body\n");
        return null;
      }
      log += " *DEFAULT " + scrollElement.nodeName + "*";
      return new(newScroller(scrollElement))(orig, scrollElement, log);
    }

    util.dump(log + " *continue*\n");
    return 1;
  }
};
