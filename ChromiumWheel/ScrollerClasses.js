/*global util*/
const ScrollerClasses = {};
// class scroller that have no edge element
// also the base class of edge scroller
ScrollerClasses.ScrollerNoEdge = class {
  constructor(orig, b, log) {
    this.target = orig;
    this.body = b;
    this.log = log;
    this.offset = 0;
    this.animation = null;
    this.edgeSize = 60;
    this.travelStartPos = 0;
  }
  backupStyle(elm, styleName) {
    let backupName = "_yass_initial_" + styleName;
    if (!(backupName in elm)) elm[backupName] = elm.style[styleName];
  }
  restoreStyle(elm, styleName) {
    let backupName = "_yass_initial_" + styleName;
    if (backupName in elm) elm.style[styleName] = elm[backupName];
  }
  get scrollTop() {
    return this.body.scrollTop;
  }
  get vpos() {
    return this.body.scrollTop + this.offset;
  }
  get bouncing() {
    return this.animation !== null;
  }
  get scrollTopMax() {
    // a rare case but transform animation cause horiz scroll bar to appear and it changes scrollHeight which we want to ignore
    if (this.animation !== null) return this._scrollTopMax;

    let clientHeight = this.body.clientHeight;
    if (this.body == document.body)
      if (this.body.clientHeight <= 20 || (document.documentElement.clientHeight < this.body.clientHeight))
        clientHeight = document.documentElement.clientHeight;

    let newValue = this.body.scrollHeight - clientHeight;
    if (this._scrollTopMax != newValue) {
      this.onMaxScrollChange();
      this._scrollTopMax = newValue;
    }
    return newValue;
  }
  get travelDistance() {
    return Math.abs(this.vpos - this.travelStartPos);
  }
  activate() {
    this.body._yass_ownedby = this;
    this.speedLog = [];
    this.onBounceBack = () => {};
    this.transformTarget = (this.body == document.documentElement) ? document.body : this.body; // do not transform HTML
    this.scrollTopMax; // call getter
    this.travelStartPos = this.body.scrollTop;
    if (getComputedStyle(this.body).getPropertyValue("scroll-behavior") == "smooth") {
      this.backupStyle(this.body, "scrollBehavior");
      this.body.style.scrollBehavior = "auto";
    }
  }
  startScroll(direction, edgeSize, onBounceBack) {
    // something special power prevents scrollTop to be just 1pixel up so
    this.body.scrollTop = (this.body.scrollTop - ((direction < 0) ? 1.0000001 : -1));
    this.travelStartPos = this.body.scrollTop;
    this.edgeSize = edgeSize;
    this.onBounceBack = onBounceBack;
  }
  adjust(delta) {
    if (this.scrollTopMax <= 0) return false;
    return this.adjustExt(delta);
  }
  adjustExt(delta) {
    // on the edge
    // (chrome issue) on zoomed out content scrollTop is rounded and might not be equal to maxscroll (stop at 1px less)
    if ((this.body.scrollTop == 0 && delta < 0)
      || ((this.scrollTopmax - this.body.scrollTop) <= 1 && delta > 0)) return false;
    return true;
  }
  onMaxScrollChange() {
  }
  scrollovercheck(hint) {
    if (this.offset == 0) {
      return (
        (hint > 0 && this.body.scrollTop < this.scrollTopMax) ||
        (hint < 0 && this.body.scrollTop > 0));
    }
    else {
      return !((this.offset > 0 && hint > 0) || (this.offset < 0 && hint < 0));
    }
  }
  stop() {
    if (this.animation) {
      this.animation.finish();
      this.animation = null;
    }
    this.offset = 0;
  }
  release() {
    this.stop();
    if (this.body._yass_ownedby == this) this.body._yass_ownedby = null;
    this.restoreStyle(this.body, "scrollBehavior");
    this.body = null;
  }
  renderBounce() {
    this.onBounceBack();
    this.stop();
  }
  render(pos, ftime, delta) {
    this.speedLog.push([ftime, delta]);
    if (this.speedLog.length > 30) this.speedLog = this.speedLog.slice(-7);

    let scrollTopMax = this.scrollTopMax;

    if (pos < 0) {
      this.offset = pos;
      this.body.scrollTop = 0;
      this.renderBounce();
      return false; // break funcWheel recursive loop
    }
    else if (pos > scrollTopMax) {
      this.offset = pos - scrollTopMax;
      this.body.scrollTop = scrollTopMax;
      this.renderBounce();
      return false; // break funcWheel recursive loop
    }
    else {
      // normal scroll - set scrollTop
      this.offset = 0;
      this.body.scrollTop = pos;
      return true;
    }
  }
};

// class scroller
ScrollerClasses.Scroller = class extends ScrollerClasses.ScrollerNoEdge {
  constructor(orig, b, log) {
    super(orig, b, log);
  }
  adjustExt(delta) {
    return true;
  }
  onMaxScrollChange() {
    // some pages have absolute positioned element longer than body (ex. side menu not by float)
    // which leads scroll height without fitting body height. causes glitchy bounce animation on bottom side.
    if (this.transformTarget == document.body) {
      this.designatedBodyHeightStyle = null;
      let bodyStyle = getComputedStyle(document.body);
      // Dont want to care about margin collapsing and other CSS layout issue, so use boundingClientRect.bottom and scrollTop
      // to know bottom edge of body element.
      let diff = this.body.scrollHeight - this.body.scrollTop
        - Math.round(document.body.getBoundingClientRect().bottom + util.CSSToFloat(bodyStyle, "margin-bottom"));
      if (diff > 0) this.designatedBodyHeightStyle = (util.CSSToFloat(bodyStyle, "height") + diff) + "px";
    }
  }
  bouncingAnimationCurve(speed, edgeSize, edgeTime) {
    speed = (isNaN(speed) || speed <= 0) ? 0.0001 : speed;
    let offsetBackward = util.range(0.2 / speed + 0.05, 0.08, 0.25); // heuristic adjustment (keep backward bounce speed consistency)
    let f = (Math.max(0.1, speed) / ((edgeSize * 2) / (edgeTime * offsetBackward)));
    let fedge = Math.min(1.0, f);
    let ftime = fedge / f;
    // curve is for 0.9143 speed ( (edgesize * 2) / (duration * offsetBackward) )
    return {edgeSize: edgeSize * fedge, duration: edgeTime * ftime, offsetBackward: offsetBackward,
      easingForward: "cubic-bezier(.3, .7, .65, 1.0)",
      easingBackward: "cubic-bezier(.35, .0, .45, 1.0)"};
  }
  // commit bouncing animation
  renderBounce() {
    if (this.animation) this.animation.finish();

    let speedLog = this.speedLog.slice(-7);
    this.speedLog = [];
    let sum, direction;
    let restoreHtmlHeightStyle = () => {};
    let restoreBodyHeightStyle = () => {};
    if (this.offset < 0) {
      sum = speedLog.reduce((i, e) => (e[1] < 0) ? [i[0] + e[0], i[1] - e[1]] : i, [0, 0]);
      direction = 1;
    }
    else { // if (offsetScroll > 0)
      sum = speedLog.reduce((i, e) => (e[1] > 0) ? [i[0] + e[0], i[1] + e[1]] : i, [0, 0]);
      direction = -1;
      // some pages use height:100% on html to make a too short content fit to viewport.
      // but this causes layout collision with negative y translation of body.
      if (this.transformTarget === document.body) {
        restoreHtmlHeightStyle = () => { this.restoreStyle(document.documentElement, "height"); };
        this.backupStyle(document.documentElement, "height");
        document.documentElement.style.height = "auto";
      }
      // and longer absolute position element problem
      if (this.designatedBodyHeightStyle) {
        restoreBodyHeightStyle = () => { this.restoreStyle(document.body, "height"); };
        this.backupStyle(document.body, "height");
        document.body.style.height = this.designatedBodyHeightStyle;
      }
    }

    // commit animation
    let speed = sum[1] / sum[0]; // distance / time
    let animInfo = this.bouncingAnimationCurve(speed, this.edgeSize, 700);
    let anim = this.transformTarget.animate([
      {transform: "translateY(0px)", easing: animInfo.easingForward },
      {transform: "translateY(" + (direction * animInfo.edgeSize) + "px)", easing: animInfo.easingBackward, offset: animInfo.offsetBackward },
      {transform: "translateY(0px)"}], {duration: animInfo.duration, fill: "none"});
    anim.onfinish = () => {
      this.offset = 0;
      this.animation = null;
      restoreHtmlHeightStyle();
      restoreBodyHeightStyle();
    };
    // calculate aproximate time offset from current position over the edge (assume curve of anim as 0.45 tilt linear)
    anim.currentTime = animInfo.duration * animInfo.offsetBackward * Math.min(0.5, (Math.abs(this.offset) / animInfo.edgeSize)) * 0.45;
    this.animation = anim;
    setTimeout(this.onBounceBack, animInfo.duration * animInfo.offsetBackward);
  }
};
