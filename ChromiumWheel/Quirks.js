/*global MatchingPatterns*/
const Quirks = {
  documentURL: new MatchingPatterns(document.URL),
  overrideWheelEvent: function() {
    return this.documentURL.test("^https://tweetdeck\\.twitter\\.com/");
  },
  ignoreKeyEvent: function() {
    return this.documentURL.test("^https://tweetdeck\\.twitter\\.com/");
  }
};
