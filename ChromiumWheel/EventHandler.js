/*global chrome,util,Scroller,Quirks,EventConverter*/
const EventHandler = {
  m_target: {
    clicked: null,
    keyboardScroll: null,
    mouseScroll: null,
    clear: function() {
      this.clicked = null;
      this.keyboardScroll = null;
      this.mouseScroll = null;
    }
  },
  m_mouseMoved: {
    m_lastValue: [-100000, -100000],
    distanceSq: function(screenX, screenY) {
      let diffX = screenX - this.m_lastValue[0];
      let diffY = screenY - this.m_lastValue[1];
      this.m_lastValue = [screenX, screenY];
      return (diffX * diffX) + (diffY * diffY);
    }
  },
  designModeChecker: {
    m_lastTarget: null,
    m_lastResult: false,
    check: function(target) {
      if (target == this.m_lastTarget) return this.m_lastResult;
      this.m_lastTarget = target;

      let b = false;
      let mode = (target.ownerDocument && target.ownerDocument.designMode) ? target.ownerDocument.designMode : "off";
      if (mode && mode.toLowerCase() == "on") b = true;
      if (target.isContentEditable == true) b = true;
      if (target.style["-webkit-user-modify"] != "") b = true;
      this.m_lastResult = b;
      return b;
    }
  },
  tickWheelChecker: {
    ticks: [],
    check: function(d, pref) {
      if (Scroller.hasTarget() == false) return; // on no scrollable area and on xul(about:blank), ev.detail is not normal value
      this.ticks.push(pref.recognizewheeltick ? Math.abs(d) : 100);
      if (this.ticks.length < 10) return;
      let tickwheel = Math.max(28, Math.min.apply(null, this.ticks));
      if (tickwheel != pref.tickwheel) {
        chrome.storage.local.set({"tick-wheel": tickwheel});
        pref.tickwheel = tickwheel;
        util.dump("tickwheel set to " + tickwheel);
      }
      this.check = function() {};
    }
  },

  m_mouseoverflowing: false,
  pref: {},
  passToParentFrame: (clonedEvent) => {}, // will be injected by main.js of child frame
  isChildFrame: () => { return false; }, // will be injected by main.js of child frame

  clearTarget: function() { this.m_target.clear(); },

  handleEvent: function(ev) {
    if (ev.altKey || ev.ctrlKey || (ev.shiftKey && ev.keyCode != 32) || (ev.metaKey && (ev.keyCode != 38 && ev.keyCode != 40))) return;

    let ev_detail = 0;

    let wheelDelta = 0;

    let passToParentFrame = (ev) => {
      let clonedevent = ["type", "detail", "deltaX", "deltaY", "deltaMode", "buttons", "wheelDeltaY",
        "screenX", "screenY", "axis", "keyCode", "shiftKey", "altKey", "ctrlKey", "metaKey"]
        .reduce((prev, elm) => { prev[elm] = ev[elm]; return prev; }, {});
      this.passToParentFrame(clonedevent);
    };

    switch (ev.type) {
      case "mousewheel":
      case "wheel": {
        if (ev.buttons != 0) return;
        // Google Chrome specific special filter for embedded PDF: it throws wheel event of html even if pdf is scrollable
        if (ev.target !== null)
          if (ev.target.type == "application/pdf" && (ev.target.nodeName.toLowerCase() == "embed" || ev.target.nodeName.toLowerCase() == "object")) return;
        if (ev.type == "wheel") {
          ev_detail = ev.deltaY * [1.0, 28.0, 500.0][ev.deltaMode];
          let wheeldeltax = ev.deltaX * [1.0, 28.0, 500.0][ev.deltaMode];
          if (Math.abs(ev_detail) < Math.abs(wheeldeltax)) return;
        }
        else {
          ev_detail = -ev.wheelDeltaY;
        }
        if (ev_detail == 0) return;
        wheelDelta = ev_detail / this.pref.tickwheel;
        this.m_target.keyboardScroll = null;
        if (this.m_mouseMoved.distanceSq(ev.screenX, ev.screenY) >= 16 || this.m_target.mouseScroll == null) {
          this.m_target.mouseScroll = ev.target;
          Scroller.refreshTarget(ev.target, ev_detail, ev);
          if (!Scroller.hasTarget()) this.m_target.mouseScroll = null;
        }
        this.tickWheelChecker.check(ev_detail, this.pref);
        if (Quirks.overrideWheelEvent()) ev.stopPropagation();
        break;
      }

      case "keydown": {
        let spacekey = false;
        switch (ev.keyCode) {
          case 38:
            ev_detail = ev.metaKey ? -3 : -1;
            break;
          case 40:
            ev_detail = ev.metaKey ? 3 : 1;
            break;
          case 33:
            ev_detail = -2;
            break;
          case 34:
            ev_detail = 2;
            break;
          case 35:
            ev_detail = 3;
            break;
          case 36:
            ev_detail = -3;
            break;
          case 32:
            ev_detail = (ev.shiftKey) ? -2 : 2;
            spacekey = true;
            break;
          default:
            // mainly for google reader shortcut issue
            Scroller.clearTarget();
            this.m_target.mouseScroll = null;
            return;
        }

        if (this.designModeChecker.check(ev.target)) return;
        if (this.m_target.keyboardScroll == null || Scroller.hasTarget() == false || this.m_target.clicked != null) {
          Scroller.refreshTarget(this.m_target.clicked || ev.target, 0, ev);
          this.m_target.keyboardScroll = 1; // not null
        }

        let tagname = ev.composedPath().shift().tagName.toLowerCase();
        util.dump("key target tagname=" + tagname);
        if (tagname == "input") {
          if (spacekey) return;
          let typeattr = (ev.target.getAttribute("type") || "text").toLowerCase();
          if (/^(text|search|password|url|tel|email|datetime|date|month|week|time|datetime-local|number|color|range|radio)$/.test(typeattr)) return;
        }
        if (tagname == "button" && spacekey) return;
        if (/^(select|textarea|embed|object|audio|video)$/.test(tagname)) return;

        this.m_target.mouseScroll = null;
        this.m_target.clicked = null;
        if (Quirks.ignoreKeyEvent()) return;
        break;
      }

      case "mousedown":
        this.m_target.clicked = ev.target;
        Scroller.softStop();
        if (this.isChildFrame()) passToParentFrame(ev);
        return;

      case "resize":
        this.clearTarget();
        Scroller.clearTarget();
        return;

      default:
        return;
    }

    if (ev.defaultPrevented) return;

    if (Scroller.hasTarget() == false) {
      if (this.isChildFrame()) {
        // send event to upper frame while nothing to scroll in this frame
        passToParentFrame(ev);
        ev.preventDefault(); // without this cause glitchy scrolling over unscrollable iframe
      }
      return;
    }

    ev.preventDefault();
    if (ev_detail == 0) return; // maybe this fixes microsoft smooth wheel problem

    let virtualPosition = Scroller.virtualPosition(); // must be ensured scroller has a target before call this

    let eventInterval = EventConverter.tick();

    // another path to upper frame
    // send scroll overflow to upper frame only by mouse
    if (this.isChildFrame()) {
      if (eventInterval > 200 || this.m_mouseoverflowing) {
        if ((virtualPosition <= 0 && wheelDelta < 0) || (virtualPosition >= 1000 && wheelDelta > 0)) {
          // with this flag, send a set of wheel events caused by user's actual single action which begins at an edge.
          this.m_mouseoverflowing = true;
          passToParentFrame(ev);
          return;
        }
        this.m_mouseoverflowing = false;
      }
    }

    if (Scroller.edgeSize > 0) {
      // ignore events in bounce animation and at near of the edges
      // for MacOS native flick
      if (eventInterval <= 200) {
        if ((virtualPosition < 5 && wheelDelta < 0) || (virtualPosition > 995 && wheelDelta > 0)) {
          return;
        }
      }
      // block only same direction event while bounce animation
      // seems here is somewhat less proper for this
      if ((virtualPosition < 0 && ev_detail < 0) || (virtualPosition > 1000 && ev_detail > 0)) {
        return;
      }
    }

    let converted = EventConverter.convert(ev_detail, wheelDelta);
    Scroller.smoothScrollBy(converted.deltaAccel, converted.deaccel, converted.bdump, converted.dump);
  },
};
