/*global util*/
const Smoother = { // eslint-disable-line no-unused-vars

  m_activeRequest: 0,
  m_jumpto: 0,
  m_jumptoremain: 0,
  m_resetsmooth: false,

  get isActive() { return this.m_activeRequest != 0; },

  animationDirection: function(scrollTop) { return this.m_jumpto - scrollTop; },

  stop: function() {
    cancelAnimationFrame(this.m_activeRequest);
    this.m_activeRequest = 0;
  },

  softStop: function(scrollTop) {
    this.m_jumpto = scrollTop;
    this.m_jumptoremain = 0;
  },

  smoothScrollBy: function(delta, deaccel, bdump, dump, scroller, bouncyEdgeSize) {
    let edgelimit = bouncyEdgeSize + 25;

    if (this.m_activeRequest == 0) {
      scroller.startScroll(delta, bouncyEdgeSize, () => { Smoother.stop(); });
      let ctm = performance.now();
      // changing scroll target's (most case it is body) "overflow" to hidden,fixed implesitly makes scrollHeight to 0
      // few pages (facebook) make scrolling target's "overflow" to fixed for preventing scrolling when showing modal dialog
      this.m_resetsmooth = false;
      let scrollTop = scroller.scrollTop;
      this.m_jumpto = scrollTop - 0 + delta + ((delta * this.m_jumptoremain > 0) ? this.m_jumptoremain : 0);
      this.m_jumpto = util.range(this.m_jumpto, -edgelimit, scroller.scrollTopMax + edgelimit);
      this.m_jumptoremain = 0;

      this.m_activeRequest = requestAnimationFrame((HRTime) => this.funcWheel(scroller, HRTime, bdump, dump, scrollTop, scrollTop, ctm - 17, ctm, 0));
    }
    else {
      this.m_resetsmooth = true;
      if (deaccel < 1.0) this.m_jumpto += (scroller.scrollTop - this.m_jumpto) * deaccel;
      else               this.m_jumpto += delta;
      this.m_jumpto = util.range(this.m_jumpto, -edgelimit, scroller.scrollTopMax + edgelimit);
    }
  },

  funcWheel: function(scroller, ctm, baseForeDumping, baseDumping, vpos, lastScrollTop, lastFuncTime, beginSmoothTime, lastDelta) {
    let stop = () => {
      this.m_activeRequest = 0;
      scroller.stop();
    };

    // if (this.m_sobj == null) {
    //   stop();
    //   return;
    // }

    let frameTime = (ctm - lastFuncTime);
    // not a common case but initial lastFuncTime can be larger than ctm
    // minimum frameTime is chosen concerning 144hz refresh rate
    frameTime = util.range(frameTime, 6.0, 34.0);

    let fordest = (this.m_jumpto - vpos);

    let bdump = baseForeDumping;
    let dump = baseDumping;

    bdump *= 2000;

    let d = 0;
    let totalTime = 0;
    let lastd = 0;

    let bdumpfunc = (time_, dump_) => util.range(time_ / dump_, 0.05, 1.0);

    do {
      let sliceTime = Math.min(frameTime - totalTime, 17.0);
      totalTime += sliceTime;
      let sliceDump = dump * (sliceTime / 17.0); // dump scaled by fragment time length
      let fordestSlice = fordest - d; // distance for dest on this time slice (signed)

      // dumping of begining
      let bDumpScale = 1.0;
      if (bdump > 0.0) {
        // reset forepart smoothing, if shoud do
        if (this.m_resetsmooth) {
          this.m_resetsmooth = false;

          for (let smoothtime = ctm; beginSmoothTime <= smoothtime; smoothtime -= 34.0) {
            let timeFromEvent = (ctm + 17.0 + totalTime) - smoothtime;
            let nextd = fordestSlice * sliceDump * bdumpfunc(timeFromEvent, bdump);
            if (lastDelta / nextd < 1.0) { // abs(lastd) < abs(nextd)
              // found new beginsmoothtime
              beginSmoothTime = smoothtime;
              break;
            }
          }
        }

        let timeFromEvent = (ctm + 17.0 + totalTime) - beginSmoothTime;
        bDumpScale = bdumpfunc(timeFromEvent, bdump);
      }

      d += fordestSlice * sliceDump * bDumpScale;
      if (lastd == 0) lastd = d;

    } while (frameTime - totalTime > 4);

    vpos += d;

    // adjust maxscroll (autopagerize or somthing dynamically add elements on m_sobj.body)
    if (scroller.adjust(d) == false) {
      stop();
      return;
    }

    let lenfordest = fordest * fordest;
    if ((!scroller.bouncing && (lenfordest <= 1.0 || (lenfordest < 100.0 && d * d < 0.2))) || scroller.scrollTop != lastScrollTop) {
      // animation finished. normal exit.
      stop();
      this.m_jumptoremain = fordest;
      return;
    }

    if (scroller.render(vpos, frameTime, d) == false) {
      // scroller object intent to break loop for some reason
      return;
    }

    lastScrollTop = scroller.scrollTop;

    this.m_activeRequest = requestAnimationFrame((HRTime) => this.funcWheel(scroller, HRTime, baseForeDumping, baseDumping, vpos, lastScrollTop, ctm, beginSmoothTime, lastd));
  }
};
