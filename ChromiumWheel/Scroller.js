/*global util,Smoother,NodeFinder*/
const Scroller = { // eslint-disable-line no-unused-vars

  edgeSize: 0,
  m_sobj: null,

  get isActive() { return Smoother.isActive; },

  hasTarget: function() { return this.m_sobj !== null; },

  // for whom interested only in offset from edges but height of a target,
  // returns -edgeSize 0 <- 500 -> 1000 +edgeSize
  virtualPosition: function() {
    let vpos = this.m_sobj.vpos;
    if (vpos < this.m_sobj.scrollTopMax / 2) return Math.min(500, vpos);
    return Math.max(500, vpos - this.m_sobj.scrollTopMax + 1000);
  },

  documentHeight: function() { return this.m_sobj.scrollTopMax; },

  pageHeight: function() { return this.m_sobj.body.clientHeight; },

  animationDirection: function() { return Smoother.animationDirection(this.m_sobj.scrollTop); },

  bouncing: function() { return this.m_sobj !== null && this.m_sobj.bouncing; },

  bouncyEdgeSize: function() { return Math.min(this.pageHeight() * 0.25, this.edgeSize); },

  setBouncyEdgeAppearance: function(opaque) {},

  travelDistance: function() { return this.m_sobj.travelDistance; },

  smoothScrollBy: function(delta, deaccel, bdump, dump) {
    Smoother.smoothScrollBy(delta, deaccel, bdump, dump, this.m_sobj, this.bouncyEdgeSize());
  },

  // initiate to stop animation
  softStop: function() {
    if (!this.hasTarget() || this.bouncing()) return;
    Smoother.softStop(this.m_sobj.scrollTop);
  },

  // stop animation immediately and release target
  clearTarget: function() {
    Smoother.stop();
    if (this.m_sobj) this.m_sobj.release();
    this.m_sobj = null;
  },

  refreshTarget: function(target, detail, ev) {
    // the reason this is here is just frequency of execution.
    // externally ordered for releasing of m_sobj
    if (target === null) { this.clearTarget(); return; }

    let newobj = NodeFinder.findNodeToScroll(target, detail, ev);
    // null : stop immediately
    // object not activated : change to it
    // 1 : do not change the target
    // 2 : event should be passed to parent frame

    if (newobj === 1) return;

    if (newobj === 2) {
      this.clearTarget();
      return;
    }

    if (newobj === null) {
      this.clearTarget();
      util.dump("N: target null\n");
      return;
    }

    if (newobj.body == this.m_sobj?.body) {
      util.dump(newobj.log + "\n");
      return;
    }

    if (this.m_sobj) {
      this.clearTarget();
      util.dump("A: " + newobj.log + "\n");
    }
    else {
      util.dump("B: " + newobj.log + "\n");
    }

    // assign scroll object
    this.m_sobj = newobj;
    newobj.activate();
    Smoother.softStop(newobj.scrollTop);
  }
};
