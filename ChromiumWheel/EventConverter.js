/*global util,Scroller*/
const EventConverter = {
  m_speed: 0.0001,
  m_lasteventtime: 0, // for speed calculation
  m_ctm: 0,
  m_sumpixelscroll: 0,
  pref: {},

  tick: function() {
    let ctm = (new Date()).getTime();
    let interval = ctm - this.m_ctm;
    this.m_ctm = ctm;
    return interval;
  },

  convert: function(ev_detail, wheelDelta) {
    let ret = {
      delta: 0,
      accel: 1,
      get deltaAccel() { return this.delta * this.accel; },
      daccel: 1,
      bdump: 0,
      dump: 0
    };

    if (wheelDelta == 0) {
      // keyboard
      let pagescroll = (Math.abs(ev_detail) == 2);
      let wholepagescroll = (Math.abs(ev_detail) == 3);

      this.m_lasteventtime = this.m_ctm;
      let step = (pagescroll) ? Math.max(0, Scroller.pageHeight() - this.pref.pagemargin) : (
                  (wholepagescroll) ? (Scroller.documentHeight() + 100) :
                    this.pref.kbdstep);
      ret.delta = (ev_detail < 0) ? -step : step;
      ret.accel = (pagescroll || wholepagescroll) ? 1.0 : (this.pref.kbdaccel / 100);
      ret.bdump = this.pref.kbdbdumping;
      ret.dump = this.pref.kbddumping;
    }
    else {
      // mouse wheel, pointing device
      let stepDelta = wheelDelta * this.pref.wheelstep;
      let evtime = util.range((this.m_ctm - this.m_lasteventtime), 1, 1500);
      this.m_sumpixelscroll += Math.abs(stepDelta);
      let speed = 0.3 * (this.m_sumpixelscroll / this.pref.wheelstep / evtime); // distance(ticks) / time
      this.m_speed *= 1.0 - util.range(((evtime - 100) / 800), 0, 0.9999); // speed loss 0.2sec offset 0.5sec to zero
      this.m_speed = Math.max(this.m_speed, speed);

      if (this.m_sumpixelscroll >= 300) {
        this.m_sumpixelscroll = Math.round(this.m_sumpixelscroll / 11);
        this.m_lasteventtime = this.m_ctm - Math.round(evtime / 11);
      }
      if (Scroller.animationDirection() * ret.delta >= 0) {
        ret.accel = util.range(this.pref.wheelaccel * this.m_speed, 1.0, 30.0);
        // accel by travel
        if (this.pref.accelbytravel > 0) {
          let k = 1666 / this.pref.accelbytravel;
          ret.accel += Math.max(0, ((Scroller.travelDistance() - k) / k));
        }
      }
      else {
        ret.deaccel = 0.92; // scale down distance betweeen here and destination
      }
      ret.delta = stepDelta;
      ret.bdump = this.pref.wheelbdumping;
      ret.dump = this.pref.wheeldumping;
    }

    if (!Scroller.isActive) {
      ret.accel = 1;
      ret.daccel = 1;
    }

    return ret;
  }
};
